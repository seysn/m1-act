# Diviser pour Régner : la ligne des toits

## Usage

Le compte rendu des questions 1 à 3 se situent dans le fichier compte-rendu.md.

Le code source des questions 4 et 5 se trouve dans le fichier main.py. Pour l'executer :

```sh
$ python3 main.py
```

Le code source utilisé pour la plateforme contest se trouve dans le fichier main_contest.py.
Pour l'executer :

```sh
$ cat input.txt | python3 main_contest.py
```

## Note
Le code utilisé pour la plateforme contest est légérement different car cette plateforme attend un ligne de toit non compacte, et un affichage different.

## Authors
Nicolas SEYS  
Hugo ALDER