Compte-rendu TP1
================

Representation d'une ligne de toits
-----------------------------------

Q1.1. La polyligne qui est une ligne de toits est :
    - (2,0)(2,5)(4,5)(4,7)(5,7)(5,0)

Q1.2. Le tuple precedent doit avoir exactement une valeur en commun et l'autre qui change avec le tuple actuel. La valeur qui change doit obligatoirement être incrementée.

Q1.3. (1,0)(1,1)(5,1)(5,13)(9,13)(9,20)(12,20)(12,27)(16,27)(16,3)(19,3)(19,0)(22,0)(22,3)(25,3)(25,0)
Pour passer de la forme non compacte à la forme compacte, il suffit de prendre un point sur 2 en commenceant par le deuxième point.
Soit (xn, yn) respectivement l'abscisse et l'ordonnée du nième point de la liste.
Pour passer de la forme compacte à la forme non compacte, il faut ajouter le point (x0, 0). Il faut ensuite ajouter après chaque point (xn, yn) le point (xn+1, yn).

Premières approches
-------------------

Q2. Pour x la longueur du tableau et y sa hauteur, la complexité est de O(xy).
    Parcourir des elements inutiles.

Entrées :
- n le nombre d'immeubles
- t les immeubles (g, h, d)

Variable :
- b le tableau de booleens

Sortie :
- La ligne de toit


```
Pour i de 0 à n
    Pour j de t[x][0] à t[x][2]
       Pour k de t[x][0] à t[x][1]
           b[j][k] = true

Tant que i < len(b)
    Si b[i][j] == true
        Ajouter les coordonnées à la ligne de toit
        Si b[i][j + 1] == true
            Tant que t[i][j + 1] == true
                Ajouter les coordonnées à la ligne de toit
                j++
        Sinon si t[i][j - 1] == true
            Tant que t[i][j-1] == true
                Ajouter les coordonnées à la ligne de toit
                j--
    Sinon
        i++
```

Q3. La complexité est de O(n)

Entrées :
- n le nombre d'immeubles
- t le tableau d'immeubles

Variables :
- c le point courant (on suppose ici qu'on peut incrementer pour avancer au point suivant)
- (g, h, d) l'immeuble courant

Sortie :
- p la ligne de toit

```
Tant que (c+1 existe && (c+1).x < g)
    c++

// On ajoute 2 points représentant la ligne verticale gauche
Si (c+1 n'existe pas)
    Ajouter (g, 0)
    Ajouter (g, h)
Sinon si ((c+1).y < h)
    Ajouter (g, (c+1).y)
    Ajouter (g, h)
Sinon
    Tant que (c+1 existe && (c+1).y >= h)
        c++

// On efface les lignes de toit dont la hauteur est inférieure à h
// Si on rencontre une ligne de toit plus haute, on inscrit les points correspondant à l'intersection de l'immeuble que l'on veut dessiner et de celui que l'on vient de rencontrer
Tant que (c+1 existe && (c+1).x <= d)
    Si ((c+1).y > h)
        Ajouter ((c+1).x, h)
        Tant que ((c+1) existe && (c+1).x <= d && (c+1).y > h)
            c++
        Ajouter ((c+1).x, h)
    Sinon
        Retirer (c+1) de la liste

Si ((c+1) n'existe pas)
    Ajouter (d, h)
    Ajouter (d, 0)
Sinon si ((c+1).y < h)
    Ajouter (d, h)
    Ajouter (d, (c+1).y)
```
