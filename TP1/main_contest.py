#!/usr/bin/env python3

#
# Classes
#

class Building:
    def __init__(self, g, h, d):
        self.g = g
        self.h = h
        self.d = d

    def __str__(self):
        return "({}, {}, {})".format(self.g, self.h, self.d)

    def create_line(self):
        l = Line([])
        l.add_point((self.g, self.h))
        l.add_point((self.d, 0))
        return l


class Line:
    def __init__(self, points):
        self.points = points

    def __str__(self):
        return '\n'.join("({},{})".format(p[0], p[1]) for p in self.points)

    def __len__(self):
        return len(self.points)

    def iterate(self):
        for p in self.points:
            yield p

    def add_point(self, point):
        self.points.append(point)

#
# Fonctions
#

def merge_lines(l1, l2):
    """ Fusionne deux lignes de toit """
    line = Line([])

    h1, h2 = 0, 0
    it1, it2 = l1.iterate(), l2.iterate()
    p1, p2 = next(it1), next(it2)

    while p1 is not None and p2 is not None:
        # On utilise le point avec le plus petit x OU le point avec le plus grand y
        # si les x sont egaux.
        # Puis on garde la valeur y du point selectionné, pour pouvoir inserer le point
        # avec x du point utilisé et y la valeur la plus grande entre h1 et h2.
        if (p1[0] < p2[0]) or (p1[0] == p2[0] and p1[1] > p2[1]):
            h1 = p1[1]
            p = (p1[0], max(h1, h2))

            # Ne pas ajouter le point si le dernier point avait la même valeur y
            if len(line) == 0 or p[1] != line.points[-1][1]:
                line.add_point(p)

            p1 = next(it1, None)
        else:
            h2 = p2[1]
            p = (p2[0], max(h1, h2))
            
            # Ne pas ajouter le point si le dernier point avait la même valeur y
            if len(line) == 0 or p[1] != line.points[-1][1]:
                line.add_point(p)

            p2 = next(it2, None)

    # Etant donné que l'on s'arrête une fois qu'un des deux tableau est fini, on doit
    # verifier s'il ne reste pas des points non exploités
    if p1 is None:
        while p2 is not None:
            line.add_point(p2)
            p2 = next(it2, None)
    elif p2 is None:
        while p1 is not None:
            line.add_point(p1)
            p1 = next(it1, None) 

    
    return line

def merge_all(lines):
    """ 
    Fusionne les deux premieres lignes de toit, puis l'ajoute à la fin et recommence
    jusqu'à ce qu'il n'y ait plus qu'un element dans la liste
    """
    while len(lines) > 1:
        l1, l2 = lines[0], lines[1]
        del lines[0]
        del lines[0]
        lines.append(merge_lines(l1,l2))
    return lines[0]

def line_completer(line):
    """ 
    Transforme une ligne compacte en non compacte
    """
    ret = [(line.points[0][0], 0), line.points[0]]
    for i in range(1, len(line)):
        ret.append((line.points[i][0], line.points[i-1][1]))
        ret.append(line.points[i])
    return Line(ret)

#
# Main
#   

if __name__ == "__main__":
    nb = int(input())
    builds = []
    for i in range(nb):
        b = [int(a) for a in input().split()]
        builds.append(Building(b[0], b[1], b[2]))
    line = merge_all([b.create_line() for b in builds])
    line = line_completer(line)
    print(line)
