# Programmation dynamique

## Méthode

- Q1
```
  +3
+1 -2
  +1 +1
  0
```

- Q2

Quand tout les successeurs sont + :
score = - max(|succ|) -1

Sinon :
score = + max(|succ_neg|) + 1

## Programmation de l’´evaluation d’une position

### Version naïve

Dans naif.py :
```
$ python3 naif.py 3 2 2 0
```

ou pour lancer des tests par defaut:
```
$ python3 naif.py
```

- Q3

naif(10, 7, 7, 3): 11
time: 0:03:21.040420
naif(10, 7, 5, 3): 15
time: 0:07:04.127456

Dans le premier cas, la carré de la mort est sur un côté. Il faut donc moins de coup pour finir la partie, donc la configuration est plus rapide.

La complexité de cette procedure est exponentielle, car pour chaque appel récursif, il y a n + m sous-problèmes.
En plus, on ne sauvegarde pas les resultats, donc il recalcule beaucoup de fois les mêmes configurations, ce qui fait que l'algorithme naif est très long.

### Version dynamique

Dans dynamique.py :
```
$ python3 dynamique.py 3 2 2 0
```

ou pour lancer des tests par defaut:
```
$ python3 dynamique.py
```

- Q4

dynamique(100, 100, 50, 50): -198
time: 0:05:59.384213
dynamique(100, 100, 48, 52): 191
time: 0:06:32.623508

- Q5

(127, 127, 0, 63)
(127, 127, 63, 0)
(127, 127, 126, 63)
(127, 127, 63, 126)

- Q6

O(m² * n²)

Etant donné qu'on sauvegarde les configurations, on passe au maximum qu'une seule fois sur un configuration precise.
Dans ce cas, si on imagine remplir le tableau des sauvegarde à 100%, on a : m² * n².


### Accélération

Dans dynamique.py :
```
$ python3 dynamique_symetrie.py 3 2 2 0
```

ou pour lancer des tests par defaut:
```
$ python3 dynamique_symetrie.py
```

- Q7

Si on tourne ou retourne la plaquette de la premiere configuration, on retrouve les autres configurations, sans avoir cassé la plaquette.

- Q8

Avant :
dynamique(100, 100, 50, 50): -198
time: 0:05:59.384213
dynamique(100, 100, 48, 52): 191
time: 0:06:32.623508
dynamique(5, 4, 3, 0): 7
time: 0:00:00.000268

Après :
dynamique(100, 100, 50, 50): -198
time: 0:00:56.060201
dynamique(100, 100, 48, 52): 191
time: 0:01:04.270781
dynamique(5, 4, 3, 0): 7
time: 0:00:00.000187


Pour des configurations simples, le code est legerement plus lent (mais en tant que simple humain on ne le voit pas !).
Tandis que pour les configurations complexes, on gagne beaucoup : pour les configurations ci-dessus, le code est 6 fois plus rapide !
