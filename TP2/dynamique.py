#!/usr/bin/env python3

from sys import argv
import datetime

tab = None
def dynamique(m, n, i, j):
    global tab

    # Init tab
    if tab is None:
        tab = [[[[None for d in range(j + 1)] for c in range(i + 1)] for b in range(n + 1)] for a in range(m + 1)]
        tab[1][1][0][0] = 0

    if tab[m][n][i][j] is not None:
        return tab[m][n][i][j]

    s = []

    # Left part
    for a in range(1, i + 1):
        s.append((m - a, n, i - a, j))

    # Right part
    for a in range(i + 1, m):
        s.append((a, n, i, j))

    # Up part
    for a in range(1, j + 1):
        s.append((m, n - a, i, j - a))

    # Down part
    for a in range(j + 1, n):
        s.append((m, a, i, j))

    pos, neg = [], []
    for c in s:
        res = dynamique(*c)
        if res > 0:
            pos.append(res)
        else:
            neg.append(abs(res))

    tab[m][n][i][j] = (-max(pos) - 1) if len(neg) == 0 else (max(neg)) + 1
    return tab[m][n][i][j]

if __name__ == "__main__":
    t1 = datetime.datetime.now()
    if len(argv) == 5:
        print("dynamique({}, {}, {}, {}):".format(argv[1], argv[2], argv[3], argv[4]), dynamique(int(argv[1]), int(argv[2]), int(argv[3]), int(argv[4])))
        print("time:", datetime.datetime.now() - t1)
    else:
        print("No argv, running defaults cases")

        # TEST 1
        print("dynamique(3, 2, 2, 0):", dynamique(3, 2, 2, 0)) # Should be 3
        t2 = datetime.datetime.now()
        print("time:", t2 - t1)
        tab = None # Reseting global tab between cases

        # TEST 2
        print("dynamique(10, 7, 7, 3):", dynamique(10, 7, 7, 3)) # Should be 11
        t3 = datetime.datetime.now()
        print("time:", t3 - t2)
        tab = None # Reseting global tab between cases

        # TEST 3
        print("dynamique(10, 7, 5, 3):", dynamique(10, 7, 5, 3)) # Should be 15
        t4 = datetime.datetime.now()
        print("time:", t4 - t3)
        tab = None

        # TEST 4
        print("dynamique(100, 100, 50, 50):", dynamique(100, 100, 50, 50)) # Should be -198
        t5 = datetime.datetime.now()
        print("time:", t5 - t4)
        tab = None

        # TEST 5
        print("dynamique(100, 100, 48, 52):", dynamique(100, 100, 48, 52)) # Should be 191
        t6 = datetime.datetime.now()
        print("time:", t6 - t5)
