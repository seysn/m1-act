#!/usr/bin/env python3

from sys import argv
import datetime

def naif(m, n, i, j):
    """
    m : colonnes
    n : lignes
    i, j : coordonnées
    """
    if m == 1 and n == 1:
        return 0

    s = []

    # Left part
    for a in range(1, i + 1):
        s.append((m - a, n, i - a, j))

    # Right part
    for a in range(i + 1, m):
        s.append((a, n, i, j))

    # Up part
    for a in range(1, j + 1):
        s.append((m, n - a, i, j - a))

    # Down part
    for a in range(j + 1, n):
        s.append((m, a, i, j))

    pos, neg = [], []
    for c in s:
        res = naif(*c)
        if res > 0:
            pos.append(res)
        else:
            neg.append(abs(res))

    return (-max(pos) - 1) if len(neg) == 0 else (max(neg)) + 1


if __name__ == "__main__":
    t1 = datetime.datetime.now()
    if len(argv) == 5:
        print("naif({}, {}, {}, {}):".format(argv[1], argv[2], argv[3], argv[4]), naif(int(argv[1]), int(argv[2]), int(argv[3]), int(argv[4])))
        print("time:", datetime.datetime.now() - t1)
    else:
        print("No argv, running defaults cases")

        # TEST 1
        print("naif(3, 2, 2, 0):", naif(3, 2, 2, 0)) # Should be 3
        t2 = datetime.datetime.now()
        print("time:", t2 - t1)

        # TEST 2
        print("naif(10, 7, 7, 3):", naif(10, 7, 7, 3)) # Should be 11
        t3 = datetime.datetime.now()
        print("time:", t3 - t2)

        # TEST 3
        #print("naif(10, 7, 5, 3):", naif(10, 7, 5, 3)) # Should be 15
        #t4 = datetime.datetime.now()
        #print("time:", t4 - t3)
